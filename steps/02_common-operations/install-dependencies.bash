#/usr/bin/env bash

apt update && \
	apt install -y gpg curl asciinema pv vim && \
	curl -qsL https://github.com/mozilla/sops/releases/download/v3.5.0/sops-v3.5.0.linux -o /usr/bin/sops && \
	chmod +x /usr/bin/sops && \
	curl -qsL https://github.com/sharkdp/bat/releases/download/v0.15.4/bat-v0.15.4-x86_64-unknown-linux-gnu.tar.gz | \
	tar -xvzf - bat-v0.15.4-x86_64-unknown-linux-gnu/bat --strip 1 && \
	chmod +x bat && mv bat /usr/local/bin/bat
