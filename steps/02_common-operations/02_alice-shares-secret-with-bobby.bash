#/usr/bin/env bash

source ../../demo-magic.sh -n

PROJECT=/project

PERSONA=Alice
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit
echo azerty | gpg --batch --import --yes --passphrase-fd 0 "$PROJECT/users/alice.gpg" > /dev/null 2>&1
gpg --import "$PROJECT/users/bobby.pub.key"

GPG_TTY=$(tty)
export GPG_TTY

clear

cp $PROJECT/steps/02_common-operations/repository/dev_a.encrypted.env .

p "# Get the value of Bobby key-id"
pe "gpg --list-keys"

p "# Add secret access to Bobby"
pe "sops --rotate --in-place --add-pgp AE0D6FD0242FF896BE1E376B62E1E77388753B8E dev_a.encrypted.env"

cp dev_a.encrypted.env $PROJECT/steps/02_common-operations/repository/.dev_a-with-bobby-access.encrypted.env

PERSONA=Bobby
DEMO_PROMPT="\e[38;5;69m$PERSONA \[\033[00m\]\$ "

gpg --batch --yes --delete-secret-key 5844C613B763F4374BAB2D2FC735658AB38BF93A > /dev/null 2>&1
echo azerty | gpg --batch --import --yes --passphrase-fd 0 "$PROJECT/users/bobby.gpg" > /dev/null 2>&1
gpg --import "$PROJECT/users/alice.pub.key" > /dev/null 2>&1

p "# Switching to Bobby"
pe "gpg --list-secret-keys"

pe "sops -d dev_a.encrypted.env"
p ""


