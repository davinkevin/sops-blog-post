We saw in the previous article [how to use SOPS](https://dev.to/stack-labs/manage-your-secrets-in-git-with-sops-g0a) to store our secrets in Git. Here, we will see some common operations required when you are using SOPS.

## Edit a secret

`Alice` would like to change the value in `dev_a` secret. To do so, she can use the `sops dev_a.encrypted.env` command to open the `$EDITOR` and allow in-place changes. 

{% asciinema 335281 %}

After the edition, the secret is encrypted back, and she can commit the file in Git. 

## Add secret access to someone else

`Alice` would like to let `Bobby` read the `dev_a` secret. To do that, she will use `sops --rotate --in-place --add-pgp <bobby-key-id> dev_a.encrypted.env` command.

{% asciinema 335288 %}

After this modification, `Bobby` can fetch modifications. He is now able to read (and modify) the secret.

## Remove secret access to someone else

`Alice` now wants to remove `Bobby` access to `dev_a` secret. She is able to do this by using the `sops --rotate --in-place --rm-pgp <bobby-key-id> dev_a.encrypted.env`. 

{% asciinema 335291 %}

After this, `Bobby` is unable to decrypt the secret anymore.

## Configure automatic key selection

Like we saw before, `sops` commands often requires references to `key-id` of people concerned by the modification... and this is error prone and hard to manage if you share access with a lot of people. 

To simplify this, the team can create a file, named `.sops.yaml` and placed it in the root of our Git repository.

```yaml
creation_rules:

  # Specific to `dev_a` env
  - path_regex: dev_a\.encrypted\.env$
    # Here, only the `Alice` key-id
    pgp: >-
      5844C613B763F4374BAB2D2FC735658AB38BF93A

  # Specific to `int` env
  - path_regex: int\.encrypted\.env$
    # Here, we have :
    # * `Alice` key-id: 5844C613B763F4374BAB2D2FC735658AB38BF93A
    # * `Bobby` key-id: AE0D6FD0242FF896BE1E376B62E1E77388753B8E
    # * `Devon` key-id: 57E6DA39E907744429FB07871141FE9F63986243
    pgp: >-
      5844C613B763F4374BAB2D2FC735658AB38BF93A,
      AE0D6FD0242FF896BE1E376B62E1E77388753B8E,
      57E6DA39E907744429FB07871141FE9F63986243

  # Specific for new env `dev_a_and_b`
  - path_regex: dev_a_and_b\.encrypted.env$
    # Here, we have only `Alice` and `Bobby` :
    # * `Alice` key-id: 5844C613B763F4374BAB2D2FC735658AB38BF93A
    # * `Bobby` key-id: AE0D6FD0242FF896BE1E376B62E1E77388753B8E
    pgp: >-
      5844C613B763F4374BAB2D2FC735658AB38BF93A,
      AE0D6FD0242FF896BE1E376B62E1E77388753B8E
```

Here, `Bobby` will create a new secret for `dev_a_and_b` env just with the command `sops dev_a_and_b.encrypted.env`. No more `--pgp <key-id>`, `sops` automatically selects the closest `.sops.yaml` file from the `CWD` (see [this](https://github.com/mozilla/sops#using-sops-yaml-conf-to-select-kms-pgp-for-new-files)).

{% asciinema 335292 %}

Keys are selected by matching a `regex` againt the path of the file, so possibilities are wide and this is simpler than using parameters in command line ! 

## Add or Remove access with `.sops.yaml`

If a `.sops.yaml` file is used, `Alice` can simplify the `add-pgp` or `rm-pgp` command previously seen. She just need to change the `.sops.yaml` and use the command `sops updatekeys dev_a.encrypted.env` to update who can decrypt the file.

{% asciinema 335295 %}

## Conclusion

Those are the most common operations required to use `sops` in your project. This is simple and still helps you to keep your secrets in sync with your code !

You can find the source code of this article, files, and scripts in this [GitLab repository](https://gitlab.com/davinkevin/sops-blog-post).
