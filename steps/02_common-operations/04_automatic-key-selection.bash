#/usr/bin/env bash

source ../../demo-magic.sh -n

PROJECT=/project

PERSONA=Bobby
DEMO_PROMPT="\e[38;5;69m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit
echo azerty | gpg --batch --import --yes --passphrase-fd 0 "$PROJECT/users/bobby.gpg" > /dev/null 2>&1
gpg --import "$PROJECT/users/alice.pub.key" > /dev/null 2>&1

GPG_TTY=$(tty)
export GPG_TTY

clear

cp "$PROJECT/steps/02_common-operations/repository/.sops.yaml" .
p "# I use the .sops.yaml to select key for me"
p "cat .sops.yaml"
bat .sops.yaml

p "# I create a new file"
pe "sops dev_a_and_b.encrypted.env"
# Execute in vim:
# secret=TN5nNuqx2hafLdEuZVL3

p "# I can decrypt it"
pe "sops -d dev_a_and_b.encrypted.env"

PERSONA=Alice
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

gpg --batch --yes --delete-secret-key AE0D6FD0242FF896BE1E376B62E1E77388753B8E > /dev/null 2>&1
echo azerty | gpg --batch --import --yes --passphrase-fd 0 "$PROJECT/users/alice.gpg" > /dev/null 2>&1

p "# Switching to Alice"
p "# I can decrypt it too !"
pe "sops -d dev_a_and_b.encrypted.env"
p ""
