#/usr/bin/env bash

source ../../demo-magic.sh -n

PERSONA=Alice
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

echo azerty | gpg --batch --import --yes --passphrase-fd 0 ../../users/alice.gpg > /dev/null 2>&1
cd "$(mktemp -d)" || exit

GPG_TTY=$(tty)
export GPG_TTY

clear

cp /project/steps/01_first-setup/repository/dev_a.encrypted.env .

p "# Check value of secret before edition"
pe "sops -d dev_a.encrypted.env"

p "# Edition of the file"
pe "sops dev_a.encrypted.env"
# Edition in VIM, new password is L7v0dsagtnLEd1G5XTMJjujYXE

p "# Check value after edition"
pe "sops -d dev_a.encrypted.env"

p "# She can commit this modification to Git"
p "git add dev_a.encrypted.env"
p "git commit -m \"chore(): change secret for dev_a\""
p ""

cp dev_a.encrypted.env /project/steps/02_common-operations/repository/dev_a.encrypted.env
