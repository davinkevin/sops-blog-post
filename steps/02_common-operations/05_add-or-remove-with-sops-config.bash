#/usr/bin/env bash

source ../../demo-magic.sh -n

PROJECT=/project

PERSONA=Bobby
DEMO_PROMPT="\e[38;5;69m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit
gpg --batch --yes --delete-secret-key 5844C613B763F4374BAB2D2FC735658AB38BF93A > /dev/null 2>&1

GPG_TTY=$(tty)
export GPG_TTY

clear

cat <<'EOF' > .sops.yaml
creation_rules:
  # Specific to `dev_a` env
  - path_regex: dev_a\.encrypted\.env$
    # Here, only the `Alice` key-id
    pgp: >-
      5844C613B763F4374BAB2D2FC735658AB38BF93A
EOF

cp $PROJECT/steps/02_common-operations/repository/dev_a.encrypted.env .
p "# I can't read the file dev_a.encrypted.env"
pe "sops -d dev_a.encrypted.env"

PERSONA=Alice
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

echo azerty | gpg --batch --import --yes --passphrase-fd 0 "$PROJECT/users/alice.gpg" > /dev/null 2>&1

p "# Switching to Alice"
p "cat .sops.yaml"
bat .sops.yaml

p "# I add the Bobby's key-id to .sops.yaml"
pe "vim .sops.yaml"
# ,
#       AE0D6FD0242FF896BE1E376B62E1E77388753B8E
#

p "# I can update keys with help of .sops.yaml"
pe "sops updatekeys dev_a.encrypted.env"

PERSONA=Bobby
DEMO_PROMPT="\e[38;5;69m$PERSONA \[\033[00m\]\$ "
gpg --batch --yes --delete-secret-key 5844C613B763F4374BAB2D2FC735658AB38BF93A > /dev/null 2>&1

p "# Switching back to Bobby"
p "# I can now decrypt the file !"
pe "sops -d dev_a.encrypted.env"
p ""
