#/usr/bin/env bash

source ../../demo-magic.sh -n

PROJECT=/project

PERSONA=Bobby
DEMO_PROMPT="\e[38;5;69m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit

## Delete all ci keys:
gpg -K --with-colon 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-secret-key {} > /dev/null 2>&1
gpg -k --with-colon 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-key {} > /dev/null 2>&1

cat "$PROJECT/users/bobby.gpg" | gpg --batch --import > /dev/null 2>&1
echo azerty | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "$(mktemp)" > /dev/null 2>&1
gpg --import "$PROJECT/users/alice.pub.key" > /dev/null 2>&1
gpg --import "$PROJECT/users/devon.pub.key" > /dev/null 2>&1

clear

p "# I have for now only other team members public keys"
pe "gpg --list-keys"

cp $PROJECT/steps/03_use-with-GitLab-ci/files/ci.public.key .
p "# I have the ci public key locally"
pe "ls -al"

p "# I will immport it"
pe "gpg --import ci.public.key"

p "# I now have one more public key available"
pe "gpg --list-keys"
