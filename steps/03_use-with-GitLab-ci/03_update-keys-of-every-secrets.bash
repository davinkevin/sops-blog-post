#/usr/bin/env bash

source ../../demo-magic.sh -n

PROJECT=/project

cd "$(mktemp -d)" || exit

## Delete all ci keys:
function remove_all_keys() {
  gpg -K --with-colon 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-secret-key {} > /dev/null 2>&1
  gpg -k --with-colon 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-key {} > /dev/null 2>&1
}

function load_all_public_keys() {
    gpg --import "$PROJECT/users/alice.pub.key" > /dev/null 2>&1
    gpg --import "$PROJECT/users/bobby.pub.key" > /dev/null 2>&1
    gpg --import "$PROJECT/users/devon.pub.key" > /dev/null 2>&1
    gpg --import "$PROJECT/users/ci.pub.key" > /dev/null 2>&1
}

PERSONA=Alice
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

remove_all_keys
cat "$PROJECT/users/alice.gpg" | gpg --batch --import > /dev/null 2>&1
echo azerty | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "$(mktemp)" > /dev/null 2>&1
load_all_public_keys


cp -r $PROJECT/steps/03_use-with-GitLab-ci/repository/. .

clear

p "# I will update .sops.yaml and update dev_a.encrypted.env"
pe "vim .sops.yaml"

# ,
#       3FF9E6938905023D25AB56EEFFEFFE9451381735

pe "sops updatekeys dev_a.encrypted.env"

PERSONA=Bobby
DEMO_PROMPT="\e[38;5;69m$PERSONA \[\033[00m\]\$ "

p "# Switching to Bobby"

remove_all_keys
cat "$PROJECT/users/bobby.gpg" | gpg --batch --import > /dev/null 2>&1
echo azerty | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "$(mktemp)" > /dev/null 2>&1
load_all_public_keys

p "# I will update .sops.yaml and update dev_a_and_b.encrypted.env"
pe "vim .sops.yaml"

# ,
#       3FF9E6938905023D25AB56EEFFEFFE9451381735

pe "sops updatekeys dev_a_and_b.encrypted.env"

PERSONA=Devon
DEMO_PROMPT="\e[38;5;184m$PERSONA \[\033[00m\]\$ "

p "# Switching to Devon"

remove_all_keys
cat "$PROJECT/users/devon.gpg" | gpg --batch --import > /dev/null 2>&1
echo azerty | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "$(mktemp)" > /dev/null 2>&1
load_all_public_keys

p "# I will update .sops.yaml and update int.encrypted.env"
pe "vim .sops.yaml"

# ,
#       3FF9E6938905023D25AB56EEFFEFFE9451381735

pe "sops updatekeys int.encrypted.env"
p ""
