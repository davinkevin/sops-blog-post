#/usr/bin/env bash

source ../../demo-magic.sh -n

PROJECT=/project

PERSONA=Devon
DEMO_PROMPT="\e[38;5;184m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit

## Delete all keys:
gpg -K --with-colon ci@domain.fr 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-secret-key {} > /dev/null 2>&1
gpg -k --with-colon ci@domain.fr 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-key {} > /dev/null 2>&1

cat $PROJECT/users/devon.gpg | gpg --batch --import > /dev/null 2>&1
echo azerty | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "$(mktemp)" > /dev/null 2>&1
gpg --import ../../users/alice.pub.key > /dev/null 2>&1
gpg --import ../../users/bobby.pub.key > /dev/null 2>&1

clear

p "# I will create a key-pair for the GitLab CI 🦊"
pe "gpg --full-gen-key"
# Doing some operation
# 1
# <enter>
# 5y
# y
# continuous-integration
# ci@domain.fr
# O
# 7jg5T9QBs6A8gDHGQaybDS
#

gpg -K --with-colon ci@domain.fr 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-secret-key {} > /dev/null 2>&1
gpg -k --with-colon ci@domain.fr 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-key {} > /dev/null 2>&1
cat $PROJECT/users/ci.gpg | gpg --batch --import > /dev/null 2>&1
echo azerty | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "$(mktemp)" > /dev/null 2>&1

p "# Extract private/public keys for futur usage"
pe "gpg -o ci.key --armor --export-secret-keys ci@domain.fr"
p "cat ci.key"
bat ci.key

p "# Extract public key separately"
pe "gpg -o ci.public.key --armor --export ci@domain.fr"
p "cat ci.public.key"
bat ci.public.key

p ""
cp ci.key ci.public.key $PROJECT/steps/03_use-with-GitLab-ci/files/
