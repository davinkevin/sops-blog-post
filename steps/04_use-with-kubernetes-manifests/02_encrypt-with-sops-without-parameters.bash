#/usr/bin/env bash

source ../../demo-magic.sh -n
source ../functions.sh

PROJECT=/project

PERSONA=Devon
DEMO_PROMPT="\e[38;5;184m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit

remove_all_keys
log_as_devon

clear

mkdir -p int
cp ${PROJECT}/steps/04_use-with-kubernetes-manifests/repository/app-secret.yaml int/

p "# I have the YAML file in clear"
p "cat int/app-secret.yaml"
bat int/app-secret.yaml

p "# I use SOPS to encrypt the YAML file"
pe "sops --encrypt --in-place --pgp 57E6DA39E907744429FB07871141FE9F63986243 int/app-secret.yaml"

p "# I check the result of the encryption"
p "cat int/app-secret.yaml"
bat int/app-secret.yaml

p "# I think SOPS encrypts too many things 🤦‍♂️"
