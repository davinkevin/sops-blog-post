#/usr/bin/env bash

source ../../demo-magic.sh -n
source ../functions.sh

PROJECT=/project

PERSONA=Devon
DEMO_PROMPT="\e[38;5;184m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit

remove_all_keys
load_all_public_keys
log_as_devon

clear

mkdir -p int
cp ${PROJECT}/steps/04_use-with-kubernetes-manifests/repository/app-secret.yaml int/
cp ${PROJECT}/steps/04_use-with-kubernetes-manifests/repository/.sops.yaml .
cp ${PROJECT}/steps/04_use-with-kubernetes-manifests/vim/add-encrypted-keys-param-to-sops-config.vim-script .

p "# I have the YAML file in clear"
p "cat int/app-secret.yaml"
bat int/app-secret.yaml

p "# I edit the configuration to add the parameter"
p "vim .sops.yaml"

vim -u "add-encrypted-keys-param-to-sops-config.vim-script" \
    -c "call Execute() | execute 'wq'" \
    .sops.yaml

p "# I can encrypt it as usual"
pe "sops --encrypt --in-place int/app-secret.yaml"

p "# I check the result of the encryption"
p "cat int/app-secret.yaml"
bat int/app-secret.yaml -r :6

p "# Still, only $.data.* are encrypted 🎉"
p "# I can add a secret inside this file directly"

#  another-secret: "dk5ORUJ6a2E2SDJyNFBnb0FPV3VuTQ=="

pe "sops int/app-secret.yaml"
p "cat int/app-secret.yaml"
bat int/app-secret.yaml -r :7

p "# Secret edition is very simple, even encrypted 🤩!"
