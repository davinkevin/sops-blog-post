#/usr/bin/env bash

source ../../demo-magic.sh -n
source ../functions.sh

PROJECT=/project

PERSONA=Devon
DEMO_PROMPT="\e[38;5;184m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit

remove_all_keys
log_as_devon

clear

mkdir -p int
cp ${PROJECT}/steps/04_use-with-kubernetes-manifests/repository/int.encrypted.env .

p "# I decrypt the secret"
pe "sops --decrypt int.encrypted.env > int/app-secret.env"
p "cat int/app-secret.env"
bat int/app-secret.env

p "# I use the kubectl command to generate the secret in yaml format"
pe 'kubectl create secret generic app-secret --from-env-file=int/app-secret.env --dry-run="client" -o yaml > int/app-secret.yaml'
p "cat int/app-secret.yaml"
bat int/app-secret.yaml

p "# I can deploy it to my cluster directly"
p "kubectl apply -f int/app-secret.yaml"
echo "secret/app-secret configured"
