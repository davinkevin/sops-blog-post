#/usr/bin/env bash

source ../../demo-magic.sh -n
source ../functions.sh

PROJECT=/project

PERSONA=Devon
DEMO_PROMPT="\e[38;5;184m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit

remove_all_keys
load_all_public_keys
log_as_devon

clear

mkdir -p int
cp ${PROJECT}/steps/04_use-with-kubernetes-manifests/repository/app-secret.yaml int/
cp ${PROJECT}/steps/04_use-with-kubernetes-manifests/repository/.sops.yaml .

p "# I have the YAML file in clear"
p "cat int/app-secret.yaml"
bat int/app-secret.yaml

p "# I will use the --encrypted-regex parameter to encrypt specific keys"
pe "sops --encrypt --in-place --encrypted-regex '^(data|stringData)$' int/app-secret.yaml"

p "# I check the result of the encryption"
p "cat int/app-secret.yaml"
bat int/app-secret.yaml -r :6

p "# Only keys in $.data.* are encrypted 🎉"
