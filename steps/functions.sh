
function remove_all_keys() {
  gpg -K --with-colon 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-secret-key {} > /dev/null 2>&1
  gpg -k --with-colon 2> /dev/null | grep fpr | cut -d":" -f 10 | xargs -L 1 -I{} gpg --batch --yes --delete-key {} > /dev/null 2>&1
}

function load_all_public_keys() {
    gpg --import "$PROJECT/users/alice.pub.key" > /dev/null 2>&1
    gpg --import "$PROJECT/users/bobby.pub.key" > /dev/null 2>&1
    gpg --import "$PROJECT/users/devon.pub.key" > /dev/null 2>&1
    gpg --import "$PROJECT/users/ci.pub.key" > /dev/null 2>&1
}

function log_as_devon() {
  cat "$PROJECT/users/devon.gpg" | gpg --batch --import > /dev/null 2>&1
  echo azerty | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "$(mktemp)" > /dev/null 2>&1
}

function log_as_alice() {
  cat "$PROJECT/users/alice.gpg" | gpg --batch --import > /dev/null 2>&1
  echo azerty | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "$(mktemp)" > /dev/null 2>&1
}
