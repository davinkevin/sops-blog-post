#/usr/bin/env bash

source ../../demo-magic.sh -n

PERSONA=Bobby
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

echo azerty | gpg --batch --import --yes --passphrase-fd 0 ../../users/bobby.gpg > /dev/null 2>&1
gpg --import ../../users/alice.pub.key
cd /tmp || exit

GPG_TTY=$(tty)
export GPG_TTY

clear

p   "# Bobby fetches files committed by Alice"
p   "git pull"
cp /project/secrets/dev_a.encrypted.env .
pe  "ls"

p   "# Bobby has his own public key and the one from Alice"
pe  "gpg --list-keys"
p   "# Bobby has only its private key"
pe  "gpg --list-secret-keys"
pe  "sops -d dev_a.encrypted.env"
p ""

PROMPT_TIMEOUT=1
wait
