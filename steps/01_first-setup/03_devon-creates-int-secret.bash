#/usr/bin/env bash

source ../../demo-magic.sh -n

PERSONA=Devon
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

echo azerty | gpg --batch --import --yes --passphrase-fd 0 ../../users/devon.gpg > /dev/null 2>&1
gpg --import ../../users/alice.pub.key
gpg --import ../../users/bobby.pub.key
cd /tmp || exit

GPG_TTY=$(tty)
export GPG_TTY

clear

p   "# Devon has everyone's public keys in the team"
pe  "gpg --list-keys"
p   "# But he has only his private key"
pe  "gpg --list-secret-keys"

p   "# Devon creates a secret"
pe  'echo "secret=5Tz2QNxki789YFDa" > int.env'
p   "# Provides public key id of Alice, Bobby and Devon to sops for encryption"
pe  'sops --pgp 57E6DA39E907744429FB07871141FE9F63986243,5844C613B763F4374BAB2D2FC735658AB38BF93A,AE0D6FD0242FF896BE1E376B62E1E77388753B8E -e int.env > int.encrypted.env'
pe  "cat int.encrypted.env"

p "git add int.encrypted.env"
p "git commit -m \"chore(): add secret for int\""

PROMPT_TIMEOUT=1
wait
