#/usr/bin/env bash

source ../../demo-magic.sh -n

PERSONA=Alice
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

echo azerty | gpg --batch --import --yes --passphrase-fd 0 ../../users/alice.gpg > /dev/null 2>&1
cd /tmp || exit

GPG_TTY=$(tty)
export GPG_TTY

clear

p "# Alice creates a secret"
pe 'echo "secret=Hb4175Z9uAZcmbLE" > dev_a.env'
p "# Alice only has her own private key"
pe "gpg --list-secret-keys"
pe "sops -pgp 5844C613B763F4374BAB2D2FC735658AB38BF93A -e dev_a.env > dev_a.encrypted.env"
pe "cat dev_a.encrypted.env"
p "# The file is now encrypted and only readable by Alice"
pe "sops -d dev_a.encrypted.env"
p "# She can commit it to Git"
p "git add dev_a.encrypted.env"
p "git commit -m \"chore(): add secret for dev_a\""
p ""

