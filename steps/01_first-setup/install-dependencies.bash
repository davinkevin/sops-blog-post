#/usr/bin/env bash

apt update && \
	apt install -y gpg curl asciinema pv && \
	curl -qsL https://github.com/mozilla/sops/releases/download/v3.5.0/sops-v3.5.0.linux -o /usr/bin/sops && \
	chmod +x /usr/bin/sops
