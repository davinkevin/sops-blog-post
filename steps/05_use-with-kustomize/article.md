
In previous parts, we see how to manage our secrets in Kubernetes format directly from Git with SOPS. For that we use the standard Kubernetes format, and some SOPS parameters to encrypt only some keys. We will go further here and see how to do that with if we want to use `kubectl -k` or `kustomize`.

*DISCLAIMER* If you want to discover `kustomize`, I've written an article about it available [here](https://dev.to/stack-labs/kustomize-the-right-way-to-do-templating-in-kubernetes-3ohp).

# Simpler solutions

We will use here the solution provided by `kubectl` / `kustomize` to generate a secret from a secret generator (see the [official documentation about it](https://kubernetes.io/docs/concepts/configuration/secret/#creating-a-secret-from-a-generator)).
This solution induces two steps, one to decrypt the secret and another to produce the `YAML`. 

## 📄 With files

`SecretGenerator` allows us to include secret from files. So, we can use `SOPS` to decrypt the file, and then we can use the `files` directive to include them into manifests.

`Devon` has the following files: 

```yaml
# kustomization.yaml
secretGenerator:
- name: app-secret
  files:
  - secret # sops encrypted file
  - another-secret # sops encrypted file
```

`secret` file before encryption: 

```text
TlZSNk1sRk9lR3RwTnpnNVdVWkVZUT09
```

And `another-secret` file before encryption: 

```text
CWNRUt3MPSTX3TizkhX2GVh5pN
```


After encryption with command `sops -i -e secret`, the file look like this: 

```json
# secret file
{
	"data": "ENC[AES256_GCM,data:FUiwHLcn9X1Js+4w5CxQ4Qh+SF3VC4AHGcxT9W3Taqmy,iv:Jh/QcOpcWelFr8cwpv2VtzJQ8/67aam9peVGImHZQdg=,tag:rzGayLJ1Blcymkk7R/Iq9A==,type:str]",
	"sops": {
		"kms": null,
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-06-20T10:12:05Z",
		"mac": "ENC[AES256_GCM,data:3aHK0NLSTT+XdFi44y4xCAfoXy+1PSc+FVTkBe8EpGCl3HlROoUbdjj/nDJTaAHQBctGg1E2U0pSgY2cIx/tWHR61XCtAZf59CL2pzMNeSRSuwJr1Atqq0ltonk08VWDVQ4DpVWBRvUPl5oxAvZpPmSk0LeuHVGeqXO2cTNa5gs=,iv:zUpUHeXghIs+/9hsHQuZtZ5UcgBzC/sF1ccJ6JHzN/U=,tag:rcwzfioUgN6H4wLcZtcrQw==,type:str]",
		"pgp": [
			{
				"created_at": "2020-06-20T10:12:04Z",
				"enc": "-----BEGIN PGP MESSAGE-----\n\nhQGMA4Hzarga0atVAQv9EQ4B8KqC41L1h4N+1jpApv/wHeDUMzsp1q5VisRndjHU\ncqmE0YtaJsPbIAMt1cdGZr+koZG5PH25Q6bby4lph3zMFQHZhyrKnGzMftsbCE92\nDPK64vmGQ1QLpJ/3897acu1NtKJicigi14Dr18ujv9kDG4HV1EeqI8o0ylycpUDr\nVVctbUP7WNKd23ShXTymVOJjjNgH4fZxoCHXnf5ndEaVKGcM6wLkPO5VtZBI0D1N\ncyy+UxWV3fZRXqjWx/qFwVGfs9wayRSy16WfSlFCNFzM4bztAxb+bLzNPWVQR4Q3\nSf7eDPVbPHF2VWvmNsODVkg8kr+flf0yKD2T1BOZ9uc+fQiZW6FQYY7qP9fSgPjk\n8Bto7rtqbcCqyQE7i7E7Xuw6hVjw8dB3nIUnr5WcLD7uuHOFTfk1YNPFV+DLdT4L\nch4bFkYLtlAz1EGavZuuGwe6vod/anS3BQyp1dNc34Z4Xoc2fZ7G3oeBCXzVlFK4\nbYLba8pAfYfVQOoMKepp0l4BtV7JTcdNWB+hCCRfPy1nAmWS8SJYvZqFhPzL9VrY\nL1kPWZ74Ooz6H1srlnNHR4YmxUHqtkHnkDv2GADZxvq0JPpKSv0fpHUi2cCnPO6d\n7VQQaUr53Gl2blMJJPBg\n=MdW8\n-----END PGP MESSAGE-----\n",
				"fp": "5844C613B763F4374BAB2D2FC735658AB38BF93A"
			},
			{
				"created_at": "2020-06-20T10:12:04Z",
				"enc": "-----BEGIN PGP MESSAGE-----\n\nhQGMA+itJvd1gkukAQv7BzY7dpzpafy1CPpyEaUyZLHkr32KPWypU+x5Mgs1BXFo\n7+Zo92bmnidK6SAgMTJFJZlZP/81AM9s9VYXkbTmpS6OaKgloPsCe4/TQYA7ktAa\nc9mRAESQSt1gDX0xupfWqdmS8yUqnAM6bULNvRkwFgV8Fjz9014sE9yM5DZckLfY\n9FxinBvEfnB2kg/8f82hu7/JIqbvc5uweUCfRglqORHx9L6wrx0suGnBeyWacNB8\nF3Dw4ICMxOBLrvohomqjGQqX7uzmYF8akZVl1vqtHz6vLRu818NIpap9xuyPMzpJ\niXiC+NthfVCAAlKOtUxWMSC+ptZu2JtPaU+WALEwLhAGj26UVsghn1w3u2cEtNwi\n09odd63jt6vXyT7KXPPyJN5stSuHZnRveN5pmXXY/+ZE4VNQXxBW8XzEW1jXJNWH\n8153wy1ObJUG5vOftt8L/NjpYEOBh9TRK+W+DIjGXCcunAHJ2MYH4JRQCvZvinZK\nsw27mdhGe1MTyfPUsLvp0l4BxIvJoO/ac/JcsWwUZBhNDynNzOObL++E0TMoJUmE\n69Gn3UmPwNcFBVHjZRBZh2IpYL5J0Hp+SBxQtKf/XI5AuLQ3jL504teiXt1YuWf4\n9JiEUVhwnGheaWIIWHCc\n=cDXu\n-----END PGP MESSAGE-----\n",
				"fp": "AE0D6FD0242FF896BE1E376B62E1E77388753B8E"
			},
			{
				"created_at": "2020-06-20T10:12:04Z",
				"enc": "-----BEGIN PGP MESSAGE-----\n\nhQGMA1ZWtaFCHvLMAQv+NkbxKcDPgNdtTs/n6OaXiFpCgCPRsjuXOmCFaZou33Od\ntNCPTHeAiMseq+Hxl3c0cdLh8wj1C5Sgqc6OBGaTOki3ZKfWA7H6DyPlbFR2d+af\nTZRQc6j7uGISJkPRVGQ5X5rJ1BcoT19rhUQm4vr3x3LHHbLvRXccKugiI7fxpDo4\nrVl7Dc9jhwz8XlV1WdalBeXvupvgxV+6aMqsnt6TFKc0vS/ECSRNoGD/cp2ZnEb8\nNRgIQXH7jv6cYSl1is7VIHvRIxyCukRznlQc6Bhcu/nDI2+vPJEIdiIX8qgG/AF7\nlfFPg8S6FALWcjNmCZksInIGltAIaQir0D2RxPfH7JdYu34c0TwgZCjMow1fBMiS\n82sa9cTWQStGh6lJALTlGWTmZeDtbL22+NX0tG0gtVGlnAmJqZbDCWunL61qrt1n\n1vvAPiYM/KZJOUibn5/gVyyb8PuVyY5Y0GahiTPPXL5h2gFo11xYpHsA9Ir0r9iB\nB2/9gyR8dqaI6xoLWJnc0l4BDdKoO+4+hhkkSneEC5FG6p0u0a2E+bjkfNKGuPas\nEnlIaWEYPh4Ndz2Km/GbII0kOuXvxdt9fxBTmB2k7/weDusUhNYY00sNxLd6rGW4\niQeX7+n5ajUO1efzqmXM\n=B2bM\n-----END PGP MESSAGE-----\n",
				"fp": "3FF9E6938905023D25AB56EEFFEFFE9451381735"
			},
			{
				"created_at": "2020-06-20T10:12:04Z",
				"enc": "-----BEGIN PGP MESSAGE-----\n\nhQGMA5/a4uV8wP1EAQv/YmHFgZNe/FgrL9etsny96gztis5qnIDCus+GdZam6j+K\nmVJ21tt5lqqPi3eXm/1OyEQKBgYjQzjDvLNFJRpQMyxogAgD062jKOOHooZWsGaA\nuhQ8Pz05pzMAdmCS2YBQmKKBHbzcue8g0iiJCKd/LAr5Ude8VTAa+Z/Re5fbZAK7\nY7UX/uz7dg0msAF8PyYxovZG907Dsin0FC/ObAcyL5GW24YOWeEHp5VCwpnyWa6v\nVr3uopiWn0n9J1sqhlb9ESq/M5qz8tqBLh1Z0yw8en6Bite+kEiDNW1o16XH4zKA\nB+SN0R/jS5uutijrneAUOQulYN148W9md1rvbL5U9VT5pJXEKPj55rSogvQkv6tK\nPH1m4WiEu7GeujO4oTD2gL5k17c5VzSLYDg3QmkYsJqLh0Z6/DvYELTKo9icMhrb\nZUj2DhTB9RtctJAbfGhZHeRp+V2Lt6at3c/bt7lINAqkEAEUqymrRu9CZwhdDcM9\nzDoanJrDqAInhUyYo7sq0l4Bbd0eyO2FNqsHTlwASkSNgP3bFdSGL5u2t4WlK+gx\nyQd/AZO8C1dcd5VWLi90j6gYe/8zrxrfnnzY1E0CvYRL21YxI2lUeS7+7EAnGmXz\nvw3ZXpnrNX44jC4RZ8FX\n=NTXR\n-----END PGP MESSAGE-----\n",
				"fp": "57E6DA39E907744429FB07871141FE9F63986243"
			}
		],
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}
```

```json
# another-secret file
{
	"data": "ENC[AES256_GCM,data:5MqAdBMFuvXolRkAiiRZzEEaOt6Tm7HU1aOd,iv:7Z7DGCzH9pQJfuMKs9/BJWsHNfK2RX+xg4/GZgQXWxg=,tag:uXwTc+2fzXmixc27RuhhCQ==,type:str]",
	"sops": {
		"kms": null,
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-06-20T10:12:08Z",
		"mac": "ENC[AES256_GCM,data:HMjZeBiuiN5oW6Z56Sd4O1+1stR7EyO1cNfAmlEUL8XSR97vSJ8mD+It0yZf7Y2/qcTBi1TQS+0Ic6YRL5RycBbRXzCwdVdAUsrQ0netosv1Zcz+jF+sRwzbtJY72hVA16XnAB/vGOBqdAOzzYt60yMI2Hw56AYvK2tnYihGJdk=,iv:bPdVIcUaNifmavFmcHroVU24dj6gqz3jLZHUIKupt5o=,tag:XucX0NIdbXxN3mNXE08Q8Q==,type:str]",
		"pgp": [
			{
				"created_at": "2020-06-20T10:12:07Z",
				"enc": "-----BEGIN PGP MESSAGE-----\n\nhQGMA4Hzarga0atVAQwAmfkQySL6NV4gJL57RKTgxsJA9VYCZeww4vhZcCCNy1pN\nzJF97UZSjrO9Pn5LZKgAHi4Ao0RANCH5S2+Se8t3t43SktJdN+TEBIG9gWa3WJ4W\nA0yyR1Xx/bD81ogYmU1CNBxbxF4QO7wZcPtBwhN7whDDLsSvd1ETOR0ZifhhGXDw\nWMieRpf3VannmCu8BugQBqYILyHCwkL/PFzZhVjBT08Vvy9EJwd0oSOKQ4LajDli\nhJkiWwyzoyLmwqZDxSOhOhjF3FHc3cowAwrNDnXs0W1k+NZE+97M96Bkd4JcRvdX\n6K7zD9oKQMT8jUToD6qyzyFGdGOnqh9odTInRDhcWGahbp8vkBsGPWrLFVxboRUy\n1yT92AbWjfLsmhkPQOfpHZcy5zqg8/lrnuYtuw7i5v3OEGjEOoJA5ddjB16roX3I\nJbyHCgrO+60dbDLFKDyYyt9kNBwo7tsFwVdoCtLdvs1/y4jwvRGR9udLaapZV/3H\nzOwFzDpLDblM/eMjIB4G0l4B0dFU0EpjTwX6XDOjf2NVDJYhIu9hwBbqQiTQ2Fhe\nlfVdSLMBwsgTmYmZsTz6Vm4QdwOY9gfNhZm6wrutJ7ZqKb0mZCYi5qtZIa96C2cG\n/pRUsrtTnp8gZao/XjAR\n=Lj0c\n-----END PGP MESSAGE-----\n",
				"fp": "5844C613B763F4374BAB2D2FC735658AB38BF93A"
			},
			{
				"created_at": "2020-06-20T10:12:07Z",
				"enc": "-----BEGIN PGP MESSAGE-----\n\nhQGMA+itJvd1gkukAQwAhdEWK1TK+FsnqQAsD5KVCYNxySWfDUqoRgD5QWI6Axnm\nTXo9d5g5qUsz6NrSjMIMQEZ4injIGj0aezxrrAQ+A5h2KnNNc76/Mhy1HdvrlnRz\nqhP0pfwyB0MFyNEoD2nJoGONzg69Ob2mMBULsVxja4p+nl7V9xcNe8o7bgfbdobJ\nYtkgNEJxfahMamQecED8/r7rUhbxxuc3yYx4ijU96dcjpnPl2HpXiD9kUVImMRuQ\nwGdV1GrUYBYFb5U/PgD24Vz2V1D5hIGifbG5MLa0ddOkiKAlz6IXj8HKbR6nNklK\nTGhs9FunpGuwgowef3BMwrKmQocvsHRgrv6Tu/sndwh7ThnV2tH0yoYOz3vaM57+\n1VDGMZSe9QHJjsgaU36lgJBgq4SfzYeWs8zE08QzjZe07iAg6YKms2M+giZ0FDNx\nsHwtCf6p432NTYokYd1NHS09c8s+fyxr271muNC5Q10+NltVJNRYuqfNPdZ9A9Aw\nUBvdyLs9X6fSs+0yPCyq0l4BbF3pL5KfR+lRXkwZmqFr5yUfkaykCpF1sIGwHbfj\n5VAnzcwTgcpSFdc9rqVq4E+HH4V5DZYbXTvqQ9vJGOZeqwUdhYF5NpRTDntzbqoe\n2PolvaOWzvbCGu+DP4Ga\n=6Tfo\n-----END PGP MESSAGE-----\n",
				"fp": "AE0D6FD0242FF896BE1E376B62E1E77388753B8E"
			},
			{
				"created_at": "2020-06-20T10:12:07Z",
				"enc": "-----BEGIN PGP MESSAGE-----\n\nhQGMA1ZWtaFCHvLMAQv+Ndwgiv2ch5F/3FcIEtzqlxoq4yuy4L/xzD9Brhoqfgqx\nZPQsrSIez93SlR7JBBGmSaPZiCeUtNuQapi70f25LugGb3HHNVs6Brss05AabTUJ\nWMNjlMBl7uk03bGtx83xNoJAnI8BGzkmrE5JlY3bYE+fTfzxY3CP8L0Tf/7lzbKy\n9uTF3Jf3sWWX0bvSJY7KKuELImTJynfBj8z29DKl6Tmt15LbwVGpqSknhsuZBMBx\nEI9JmwfPnA233jAz+f6QCDodx4GDv9+Ak5CJL9NleMBGJdG6VONrKRe8a8yNCI1k\nVyqgx9oWzpEZK9Go4eBIrbv9GpnyRDo7ULX6luqAeys0co0Cp8N/f8T20K24vEm+\nGhi1dIwcIj3gyFZZ+o7OkI/355CAdmYAldyEkBycZMV+oC7r3zyLniLRrTEzrtwt\nQZCdu/lejBoUK/fWY++L8slZjTL8jVX9lVMHKQZqmC4LMsXtoPv9uS6xlA9I8usp\nOzhrdGCiapQVfPAvZ3Fa0l4BHpAWInZcfWjYXCWfOfis+ygb/YV4I9BLRFTzRiRR\nEpug1dNDEDX3uYO5BsQBt8qMPMsH7T+XI21081oLIH95UD5iOSiYsu600whQV7XO\ng6p/T5LaVdoreMBDcyy/\n=DSoD\n-----END PGP MESSAGE-----\n",
				"fp": "3FF9E6938905023D25AB56EEFFEFFE9451381735"
			},
			{
				"created_at": "2020-06-20T10:12:07Z",
				"enc": "-----BEGIN PGP MESSAGE-----\n\nhQGMA5/a4uV8wP1EAQv/Z3Z2n4inarsPkhSQllWWG7+O7nDwdOK7c6S01f/eGBQ9\nbzwhsZmyuYQ9ReKHRpxhbLjYnQBgc8JqDHCDgznZ8aIFp4XQ3zZeZwBzWYX2D+Ok\ndXt2Ht1yd1zkhMGgrj55yjojnFZRRuNclXFZtypkVFzlU7trrBknlCjQDGIDqOS4\nZXUQ3IHeDOW7F0+wPxz96i4ZfxcZcOtRhsGGqxrMPAXRZGwgRwdOSJNC1a5iwEsc\nk9DPcluf9+1Px/eOW2lmExntqkscNCgkjgT4ACsUCmI4p1V/LUrwvkPDPCH0EE4h\n9MuBKQNuqp+gjjPwR9NOLOtoa/Njsp2V0vZsQ39Y+nfkqtE9w1bAX/R1VDABU+Bm\nml0WjT0eqGQwnZsBY4pfEvRwIS4JVQCa8sGhZXS4iax0uXgEFh4jma2uWBWKBnGd\nD9mJ53RC6Xb4eTSkvNtF2VBFtqdu0N587xhykrprxVf7q/s2RXu3haP+JmdJHfMW\nx2utciyuNIWAGm13u3Cn0l4BK5z9hlBxZfOeGAIyQIWQjYn/ZDG8smHmo8jGUPIR\nki/bs+uzo5Fnyk7OJhr0+bkQjG6weVoZJIUxZr61Tb0JhV4+xXaPVMZoZpg96Bii\n+VWaRWaE+nTfH4wrXwDT\n=svPU\n-----END PGP MESSAGE-----\n",
				"fp": "57E6DA39E907744429FB07871141FE9F63986243"
			}
		],
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}
```

{% asciinema 341419 %}

After decryption of the secret and execution of the command `kubectl apply -k . --dry-run -o yaml` or `kustomize build .`, we have the following YAML

```yaml
# Generated manifest
apiVersion: v1
data:
  another-secret: Q1dOUlV0M01QU1RYM1RpemtoWDJHVmg1cE4K
  secret: VGxaU05rMXNSazlsUjNSd1RucG5OVmRWV2tWWlVUMDkK
kind: Secret
metadata:
  name: app-secret-8m97554t4c
  namespace: default
type: Opaque
```

You see here, the `secret` and `another-secret` keys used in the file `kustomization.yaml` (`$.secretGenerator[*].files[*]`) are both filenames on the file-system, and keys in the generated manifests (`$.data[*]`). This could lead to problems if key name can't be a file (unauthorized characters for example). 

## 🗂 With `.env` files

A feature available into `kustomize` but not yet in `kubectl` (see [issue](https://github.com/kubernetes/kubernetes/issues/82905)) is the possibility to use `.env` file as secret source.
 
```yaml
secretGenerator:
- name: app-secret
  envs:
  - secrets.env
```

The `secrets.env` before encryption: 

```dotenv
secret=TlZSNk1sRk9lR3RwTnpnNVdVWkVZUT09
another-secret=CWNRUt3MPSTX3TizkhX2GVh5pN
```

After encryption with command `sops -i -e secrets.env`:

```dotenv
secret=ENC[AES256_GCM,data:LZxAINIj7/HOIqSJ/M1YTjGmjGIFk24KKEKyXxcFHvQ=,iv:OJNKetoqlJdmYKFpFsfr1ihQjpZ6nJfm8INW474EfE8=,tag:Ea8G+IGH+aORM0sePL9Njw==,type:str]
another-secret=ENC[AES256_GCM,data:OFgAiBkTzEjPwxpo0qn17wvSXm3T164f1Wg=,iv:+kvczR+vGDl6W/UG8KOl++cCKDmx3zA4cGC2vIsge7w=,tag:n7Qbs/I/qo4uy4FfCOMQkg==,type:str]
sops_pgp__list_3__map_fp=57E6DA39E907744429FB07871141FE9F63986243
sops_pgp__list_2__map_enc=-----BEGIN PGP MESSAGE-----\n\nhQGMA1ZWtaFCHvLMAQv/S/gV3liTwmA8Rm3v08kjV39mfBF66mSbjgljoVMvlgoQ\nZ6l2djvzUfzTpgdfACwVVbzQ+KWYfYWnHtOlEHKGQ1abi4Ik/WRS9J6xfzf0aPj0\n9GAy43wCqblvKLKBPHzfYXsQQLP/IA3gHgj1T0hRMx0Nbv+bHi16DEilzxyhCQQf\nzV+w0IMfF1CJrTvTgn4ohaN1KvuQcqOJNlWyr0/fIL420QYL09rQvjA20ds8Jwaq\nZ61O6XOtjyCVAB1nAYfCIOtd0t/+ApnI6Qwe5FKKcN1GrCiqJ4rt4mCK9/nLRcqg\nF1h825h5yuAWYjIyIb8zbulfLxUUN8ztbn4myIUKTcXu+Onq6dq2jaCKOceVc90m\njpXwvcDKYRdeG1HUcmOeQu9ioyt+4xGxOkWY8/hg1YJp9lA+/n4uU5OE9Su0o8LP\n71EzXfISx2PeROkY2W83e8qhzgQQNsCOI9Y5k8Tmd8ctgI4i9Pw0j1S2+sdiD5Y9\njOW7hPVLNiQ/avAkfKVr0lwBZSwkvC3d0Rbw4Wskj8/meRNMYf8DsbFOYi05ysjO\nwTvWQFYPy4cvMtaIvMKn05f+dQIm/6jwOwu6tyytKxqYuq/vEPOrUMQU72wA25eS\nTyXEd7AjE4boT/hQ9g==\n=gUpS\n-----END PGP MESSAGE-----\n
sops_pgp__list_3__map_enc=-----BEGIN PGP MESSAGE-----\n\nhQGMA5/a4uV8wP1EAQv8DQgTw5K4u3E+J9rRxiNK3AvdM+ajifrhZ/NEXu1hgj6g\nMhp8eqQoG1P7/1OJAYL1B20fTatol6oHXgpE7/+f8W6pY+wLfo4d6JSL+KSAAxJk\nWizkR87JxWZFyQB109vgZRDagPylA02yq/2XSgI47JFGVPB14/MAxJkr3O47Si3D\nc3NkqUew8jwPNAToLxM/oCnnDuEoSKmB6smSiL5UHk2/04h3PqbcwUnEPheGR9LK\neA6LVhQ/AiEtbZqAeSU46KGLVHFvQdQVKO+sAHkia21Y041tfS1HDiijTk3UxYTV\nSIB5OKln0qVdDZPYDg4y6Pc/qvj+DHUE+gDoSFOOaGPl1BMw5m9AoY/UQ+Tip66k\nBpG54b20GNmlCuI+7N2QG/lpoML4CGDP2AJ40o6KbZrlo4iwGieg9gdSAstJKlvg\nPpi/p/Fo2zyWin1Gjf/T3QYk5PjbeDkqDqyehToI90qDv8KF7ZDf/t3bz9Geitef\nt9Vg74WCXK/yNcyGAGbC0lwBaqxUf2erAGoTDBkmjjlyEGiEsIgpZzhNj85/G1Mp\n1NxM7zM+5nJGEquzXe/FEoOVX4+LRm1W8+y0T2FRxiOUhlE3anWs+576BYZ+1zZl\nourCUh/MJPpDz3R6Cg==\n=D5A/\n-----END PGP MESSAGE-----\n
sops_pgp__list_1__map_enc=-----BEGIN PGP MESSAGE-----\n\nhQGMA+itJvd1gkukAQv+NZtoRyHQfmOEA2zLM3HaK2HDofMNqcA8Gy66sOfMRwM0\nDvnyu7gcurDE75hYiY4Dd/k5wy+HE7c6fITIWZ62nGAU33RQKgWzZ/vuZX2JnzcA\n8d2XsyoBLe3KGOB75tfepYVkPuRdzDqCY+IH6bHvr9MtNW/CGL58OBXfGTKs1NnI\nSpBWPhF/8Zj0l9S5QkA5ENYGM2u/yutav1z281PNLmZmTrmZ9VYnytwwPX2dY9LS\nupyJnywKqwuw1iaFh7f8BhbuqTjNzAkmjycL8ZFLQB1uGI5CwxCGZ9a3KByNJ6Eh\nH/KVrK5rBBP09ByoIAYoiSBraejoovNFns9O2oUin8HEjv4tziuZ3KeqR51uJCS0\n31nZ/JBlx7GyJ7WGKoVsthlrOebpUuDRbmcKhzhNZT7umTCOYCHdTvGH5p6nMvsE\ngcuuqiTJjrp1WECkr1mMhQqD1Ef708Cw9TAK1LbzdHz5ePBWZ9b+FP0PpBUBHZOc\npWlzd8nYATEK7kQsRbUw0lwB5XQZZKltYrzy1n0dmI98HpqWUpx/f0BX9WRQMkgz\nmbZ8iEQIOBSexNvcM3yA9RMVaTW6M1WoHPGoQdiz4kw4gWtaKqxkK/RlBa6YPnwG\ncrToyqNOO5+HxrSIJg==\n=3esV\n-----END PGP MESSAGE-----\n
sops_pgp__list_1__map_fp=AE0D6FD0242FF896BE1E376B62E1E77388753B8E
sops_pgp__list_2__map_created_at=2020-06-20T10:30:18Z
sops_pgp__list_0__map_created_at=2020-06-20T10:30:18Z
sops_pgp__list_0__map_fp=5844C613B763F4374BAB2D2FC735658AB38BF93A
sops_unencrypted_suffix=_unencrypted
sops_lastmodified=2020-06-20T10:30:19Z
sops_pgp__list_3__map_created_at=2020-06-20T10:30:18Z
sops_pgp__list_2__map_fp=3FF9E6938905023D25AB56EEFFEFFE9451381735
sops_version=3.5.0
sops_pgp__list_0__map_enc=-----BEGIN PGP MESSAGE-----\n\nhQGMA4Hzarga0atVAQv/cIT+IM2097+hQ5NWuod9D7WamqfLTkYeCA+eQqMWRy7l\nBeUGX+JNrh8Wzmq3jAT4NP61/WZiPdX/sqmw/VKd7f+RoLoT5vvT/omHqXgt07Qv\nG0Sp/lz5KZF9d5ujiEmvOTbHZgFhA/8CzLl6rfWLzyHocdV5585vcnRK6RUa8Ezh\nUG9+nLCecq5xIAiqUfWpwdt+bznHAgb9VMW8m8T3UFDnUDa0Y/ankLJ9wIaReoVB\nTCRpJnr5fKxUw4r+kJDFxhnSg21Iw5Rx4rGJHAlDmazXGHazXD5z7f9g3ZiHxrOQ\n6WpoxZw/X/d3697viqe/yemp+CDQJUWQBolhAqfrMN95KllNo7vb7dOpNYG4PFIK\nRLNSTgg+2Ph4mE7AQHTqCdV1jPhtONApcf7CQmkQG/KqyJ5YCfwDLzfo+7+pv1Xx\nhSYEGwMVX+jBIW7ZlLsqcYUaJNQeF3DYNrfgdsTiMB7aGdhohOJvpL/771t37P2s\nyJ/rgu/K8J1InTcEfRPs0lwBeXjohJIpTOL09QgXNSQ0Dsio19VhPjWTr0QI6cAP\nYedVlwrAebeQAOkBlC8RPSUG35CYUqnSRpGHKrl03xCP0Z4TXZ+v60XphehDpdOn\nROm168cPbVF+K2BTTQ==\n=Pdik\n-----END PGP MESSAGE-----\n
sops_mac=ENC[AES256_GCM,data:kXxkiJmgK5nyDiEwStytEHwKLw5JIN1Y1xiNUAXKJLNGEk4VyZ+VkqMNBcUED8HQycHIRle550/Mtov4aVtviM9jM7MnTMIdtg/bp6f0AKiXcl5nCWt26b3JP2nvLN/COMUnvxHxW3uwhdnrsJupBMNDSVdDOv2wXbyazfuor+U=,iv:LR2dRc054UDQoWCVJ+bynri5j5zFKvE69ggLh19sXLY=,tag:nvYpc7ez5Hb4jYLNUnd9Fw==,type:str]
sops_pgp__list_1__map_created_at=2020-06-20T10:30:18Z
``` 

After decryption of the `secrets.env` and execution of the command `kustomize build .`, we have the following YAML

```yaml
apiVersion: v1
data:
  another-secret: Q1dOUlV0M01QU1RYM1RpemtoWDJHVmg1cE4=
  secret: VGxaU05rMXNSazlsUjNSd1RucG5OVmRWV2tWWlVUMDk=
kind: Secret
metadata:
  name: app-secret-4g57fkmb8b
type: Opaque
```

{% asciinema 341420 %}

Both way can be used without any problem (`files` or `envs`). This depends of the restriction on tooling you have to generate/deploy your manifests. 

# 🔧 Alternative with Kustomize Plugins

The main goal about using the `kustomize` plugin system is to be able to remove the step of files decryption before manifests creation. Thanks to that, you will be able to use `SOPS` secrets with some tools like skaffold and or others using`kustomize` under the hood. 

`kustomize` has two plugins system ([official plugin documentation](https://kubernetes-sigs.github.io/kustomize/guides/plugins/))

* native `Go` extension
* exec `shell` extension

This `native` solution is very useful because all can be done directly from `kustomize` code, and we don't need to have `SOPS` installed (for local or CI execution). 

But this solution is complex and requires compiling `kustomize` & plugins from sources 😓... with all the Go build system. There is some `SOPS` plugins available: 

* [KSOPS](https://github.com/viaduct-ai/kustomize-sops)
* [Agilicus/kustomize-sops](https://github.com/Agilicus/kustomize-sops)
* [barlik/kustomize-sops](https://github.com/barlik/kustomize-sops)
* [sopsencodedsecrets](https://github.com/monopole/sopsencodedsecrets)      

The `shell` solution is simpler to manage, but it will require installing `SOPS` or other binary to perform decrypt operation. I found only one implementation of shell plugin for `kustomize` available: [kustomize-sopssecretgenerator](https://github.com/goabout/kustomize-sopssecretgenerator)

Both solutions have another downside, it requires extracting secrets configuration outside the `kustomization.yaml` (in a `generator.yaml` file), so it multiplies files and make it harder to read / manage 😓. Depending on your needs, I advise you to use `KSOPS` or `kustomize-sopssecretgenerator`.

# 🔐 Conclusion

We have seen here a “simpler” solution to manage our secret and to use them with Kubernetes relying on the builtin Kustomize system. The plugin ecosystem is still young, so I hope integrations will be simpler in futur. 

You can find the source code of this article, files and scripts in this [GitLab repository](https://gitlab.com/davinkevin/sops-blog-post). 
