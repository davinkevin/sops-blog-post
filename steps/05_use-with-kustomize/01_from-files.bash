#/usr/bin/env bash

source ../../demo-magic.sh -n
source ../functions.sh

PROJECT=/project

PERSONA=Devon
DEMO_PROMPT="\e[38;5;184m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit

remove_all_keys
load_all_public_keys
log_as_devon

clear

cp ${PROJECT}/steps/05_use-with-kustomize/repository/files/.sops.yaml .

FIRST_SEC=TlZSNk1sRk9lR3RwTnpnNVdVWkVZUT09
ANOTHER_SEC=CWNRUt3MPSTX3TizkhX2GVh5pN

p "# I generate the kustomization.yaml"
pe "cat << EOF > kustomization.yaml
secretGenerator:
- name: app-secret
  files:
  - secret # sops encrypted file
  - another-secret # sops encrypted file
EOF"

p "# I create two secrets"
pe "echo ${FIRST_SEC} > secret"
pe "echo ${ANOTHER_SEC} > another-secret"

p "# I encrypt both files"
pe "sops --encrypt --in-place secret"
pe "sops --encrypt --in-place another-secret"
p "cat secret"
bat secret -l json
p "cat another-secret"
bat another-secret -l json

PERSONA=Alice
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

remove_all_keys
load_all_public_keys
log_as_alice

p "# Switch to Alice"
p "# I decrypt both files in place"
cp secret secret.backup
cp another-secret another-secret.backup
pe "sops --decrypt --in-place secret"
pe "sops --decrypt --in-place another-secret"

p "# I generate the yaml manifests"
p "kubectl apply -k . --dry-run=client -o yaml"
kustomize build . | bat - -l yaml

p "# The file can be applied to the cluster"
