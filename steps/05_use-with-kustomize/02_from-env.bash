#/usr/bin/env bash

source ../../demo-magic.sh -n
source ../functions.sh

PROJECT=/project

PERSONA=Devon
DEMO_PROMPT="\e[38;5;184m$PERSONA \[\033[00m\]\$ "

cd "$(mktemp -d)" || exit

remove_all_keys
load_all_public_keys
log_as_devon

clear

cp ${PROJECT}/steps/05_use-with-kustomize/repository/envs/.sops.yaml .

p "# I generate the kustomization.yaml"
pe "cat << EOF > kustomization.yaml
secretGenerator:
- name: app-secret
  envs:
  - secrets.env
EOF"

p "# I create the env file"
pe "cat << EOF > secrets.env
secret=TlZSNk1sRk9lR3RwTnpnNVdVWkVZUT09
another-secret=CWNRUt3MPSTX3TizkhX2GVh5pN
EOF"

p "# I encrypt it"
pe "sops --encrypt --in-place secrets.env"
p "cat secrets.env"
bat secrets.env

PERSONA=Alice
DEMO_PROMPT="\[\033[01;32m\]$PERSONA \[\033[00m\]\$ "

remove_all_keys
load_all_public_keys
log_as_alice

p "# Switch to Alice"
p "# I decrypt the env file"
cp secrets.env secrets.env.backup
pe "sops --decrypt --in-place secrets.env"

p "# I generate the yaml manifests"
p "kustomize build ."
kustomize build . | bat - -l yaml

p "# The file can be applied to the cluster 🚀"
